import java.util.Random;
class Die{
	private int faceValue;
	private Random random;
	
	public Die(){
		this.faceValue = 1;
		this.random = new Random();
	}
	public int getFaceValue(){
		return this.faceValue;
	}
	public String roll(){
		this.faceValue = this.random.nextInt(6)+1;
		return faceValue+"";
	}
	public String toString(){
		return "The face value of the dice: "+this.faceValue;
	}
}