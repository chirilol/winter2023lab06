class Board{
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tiles = new boolean[12];
	}
	public String toString(){
		String resultString = "";
		for(int i = 0; i<this.tiles.length; i++){
			if(this.tiles[i]){
				resultString += " "+(i+1);
			}else{
				resultString += " X";
			}
		}
		return resultString;
	}
	public boolean playATurn(){
		this.dice1.roll();
		this.dice2.roll();
		System.out.println(this.dice1.getFaceValue()); 
		System.out.println(this.dice2.getFaceValue());
		
		int sumOfDice = this.dice1.getFaceValue() + this.dice2.getFaceValue();
		if(!tiles[sumOfDice-1]){
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: "+sumOfDice);
			return false;
		}
		else if(!tiles[this.dice1.getFaceValue()-1]){
			tiles[this.dice1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: "+this.dice1.getFaceValue());
			return false;
		}
		else if(!tiles[this.dice2.getFaceValue()-1]){
			tiles[this.dice1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: "+this.dice1.getFaceValue());
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}