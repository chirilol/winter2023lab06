class Jackpot{
	public static void main(String[] args){
		System.out.println("Welcome player to the Jackpot Game!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(!gameOver){
			System.out.println(board.toString());
			if(board.playATurn() == false){
				numOfTilesClosed++;
			}else{
				gameOver = true;
			}
		}
		if(numOfTilesClosed<=7){
			System.out.println("You won the Jackpot Game!");
		}else{
			System.out.println("You lost the Jackpot Game!");
		}
	}
}